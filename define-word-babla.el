;;; define-word-babla.el --- Fetch definitions from bab.la with define-word  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Anders Johansson

;; Author: Anders Johansson <anders.l.johansson@chalmers.se>
;; Created: 2022-01-24
;; Modified: 2022-03-21
;; Keywords: wp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'define-word)
(require 'dom)
(require 'cl-lib)
(require 'subr-x)

(defcustom define-word-babla-url "https://sv.bab.la/lexikon/svensk-engelsk/%s"
  "The url to use for bab.la, include a %s for the search word.
Needs to be set before loading the library."
  :group 'define-word
  :type 'string
  :safe #'stringp)

;;;; Defining the service
(add-to-list 'define-word-services
             `(babla ,define-word-babla-url define-word-babla-parse))

(add-to-list 'define-word-displayfn-alist
             '(babla . define-word-babla-display))

(defun define-word-babla-display (str)
  "Display STR in popup buffer."
  (pop-to-buffer (generate-new-buffer "*Define word bab.la*"))
  (insert str)
  (goto-char (point-min))
  (view-mode)
  (visual-line-mode)
  (variable-pitch-mode))

(defun define-word-babla-parse ()
  "Parse output from bab.la and return formatted list"
  (decode-coding-region (point-min) (point-max) 'utf-8-unix)
  ;; TODO, understand this coding business with request ^^
  (let* ((parsed-tree (libxml-parse-html-region (point-min) (point-max))))
    (cl-loop for (title . section) in `(("Translations" . ,(dom-by-id parsed-tree "translationsdetails-sv1"))
                                        ("Examples" . ,(dom-by-id parsed-tree "practicalexamples")))
             concat
             (progn
               (define-word-babla--clean-node section)
               (concat
                (propertize title 'face 'info-title-1)
                "\n"
                (cl-loop for example in (dom-by-class section "dict-example")
                         concat
                         (progn
                           (concat
                            (string-clean-whitespace
                             (dom-texts (dom-by-class example "dict-source") ""))
                            "\n        "
                            (string-clean-whitespace
                             (dom-texts (dom-by-class example "dict-result") ""))
                            "\n\n")))
                "\n")
               ))))

(defun define-word-babla--clean-node (node)
  "Clean out unwanted elements from NODE.
Propertize strong with bold."
  (cl-loop for n in (dom-by-class node "icon-link-wrapper")
           do (dom-remove-node node n))
  (cl-loop for n in (dom-by-tag node 'a)
           do (dom-remove-node node n))
  (cl-loop for n in (dom-by-tag node 'strong)
           do (setf (nth 2 n) (propertize (nth 2 n) 'face 'bold))))



(provide 'define-word-babla)
;;; define-word-babla.el ends here
